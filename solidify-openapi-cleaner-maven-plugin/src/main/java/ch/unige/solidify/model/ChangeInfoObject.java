/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - ChangeInfoObject.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import ch.unige.solidify.process.BaseProcess;

public class ChangeInfoObject extends AttributeObject {
  public class Properties {
    private AttributeObject when;
    private AttributeObject who;

    public AttributeObject getWhen() {
      return this.when;
    }

    public AttributeObject getWho() {
      return this.who;
    }

    public void setWhen(AttributeObject when) {
      this.when = when;
    }

    public void setWho(AttributeObject who) {
      this.who = who;
    }

  }

  private final Properties properties = new Properties();

  public ChangeInfoObject() {
    super(BaseProcess.OBJECT, "The change info");
    this.properties.setWhen(new AttributeObject(BaseProcess.STRING, "The date of the executed event", BaseProcess.FORMAT_STRING_DATE_TIME));
    this.properties.setWho(new AttributeObject(BaseProcess.STRING, "The user who executed the event"));
  }

  public Properties getProperties() {
    return this.properties;
  }

}
