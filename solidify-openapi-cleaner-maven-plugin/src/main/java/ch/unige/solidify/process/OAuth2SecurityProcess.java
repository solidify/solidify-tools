/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - OAuth2SecurityProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Update tags with module name
 */
public class OAuth2SecurityProcess extends BaseProcess {
  private static final String SOLIDIFY_OAUTH2 = "{\"type\" : \"oauth2\", \"flows\" : { \"implicit\" : { \"authorizationUrl\" : \"https://localhost\", \"scopes\" : {} } } }";
  private final String securityName;

  public OAuth2SecurityProcess(String securityName) {
    this.securityName = securityName;
  }

  @Override
  public void process(JSONObject json) {
    // Add Security
    json.put("security", this.getSecurity());
    // Add Security Scheme
    this.getComponentsNode(json).put("securitySchemes", this.getSecuritySchemes());
  }

  private JSONArray getSecurity() {
    final JSONArray security = new JSONArray();
    final JSONObject auth = new JSONObject();
    auth.put(this.securityName, new JSONArray());
    security.put(auth);
    return security;
  }

  private JSONObject getSecuritySchemes() {
    final JSONObject securitySchemes = new JSONObject();
    final JSONObject oauth2 = new JSONObject(SOLIDIFY_OAUTH2);
    securitySchemes.put(this.securityName, oauth2);
    return securitySchemes;
  }

}
