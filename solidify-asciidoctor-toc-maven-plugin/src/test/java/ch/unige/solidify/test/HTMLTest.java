/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify asciidoctor TOC Extractor - HTMLTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test;

import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.unige.solidify.model.xml.xhtml.v1.XhtmlHtmlType;

@ExtendWith(MockitoExtension.class)
public class HTMLTest {
  private static String SRC_TEST_RESOURCES = "src/test/resources/";
  private static String DLCM_GUIDE = SRC_TEST_RESOURCES + "DLCM-IntegrationGuide.html";
  private static String DLCM_METADATA_HTML_SCHEMA_ORG = SRC_TEST_RESOURCES + "jaxb/dlcm-html-schema.org.xml";

  @Test
  public void unloadAsciidoctorHTMLTest() {
    final Path in = Paths.get(DLCM_GUIDE);

    try (FileReader fileReader = new FileReader(in.toFile())) {
      final JAXBContext jaxbContext = JAXBContext.newInstance(XhtmlHtmlType.class);
      final JAXBElement<XhtmlHtmlType> root = (JAXBElement<XhtmlHtmlType>) jaxbContext.createUnmarshaller().unmarshal(fileReader);
      jaxbContext.createMarshaller().marshal(root, System.out);
    } catch (IOException | JAXBException e) {
      e.printStackTrace();
      assertTrue(e.getMessage(), false);
    }
  }

  @Test
  public void unloadXHTMLTest() {
    final Path in = Paths.get(DLCM_METADATA_HTML_SCHEMA_ORG);

    try (FileReader fileReader = new FileReader(in.toFile())) {
      final JAXBContext jaxbContext = JAXBContext.newInstance(XhtmlHtmlType.class);
      final JAXBElement<XhtmlHtmlType> root = (JAXBElement<XhtmlHtmlType>) jaxbContext.createUnmarshaller().unmarshal(fileReader);
      jaxbContext.createMarshaller().marshal(root, System.out);
    } catch (IOException | JAXBException e) {
      e.printStackTrace();
      assertTrue(e.getMessage(), false);
    }
  }

  private JAXBContext getJaxbContext() throws JAXBException {
    return JAXBContext.newInstance(XhtmlHtmlType.class);
  }

}
