/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - SchemaExtraDocumentationProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.List;

import org.json.JSONObject;

import ch.unige.solidify.model.OpenApiExtraDocumentation;
import ch.unige.solidify.model.OpenApiExtraDocumentationProperties;

/**
 * Force type of all array into string
 */
public class SchemaExtraDocumentationProcess extends BaseProcess {

  private final List<OpenApiExtraDocumentation> listOpenApiExtraDocumentation;

  public SchemaExtraDocumentationProcess(String jsonExtraDocumentation) {
    this.listOpenApiExtraDocumentation = this.loadOpenApiExtraDocumentation(jsonExtraDocumentation);
  }

  @Override
  public void process(JSONObject json) {
    // Update schemas with extra info
    for (final OpenApiExtraDocumentation extraDoc : this.listOpenApiExtraDocumentation) {
      if (!extraDoc.getProperties().isEmpty()) {
        final JSONObject schemas = this.getSchemasNode(json);
        final JSONObject schema = schemas.getJSONObject(extraDoc.getSchema());
        this.iterateOverExtraDocumentationProperties(schema, extraDoc.getProperties());
      }
    }
  }

  private void iterateOverExtraDocumentationProperties(JSONObject schema,
          List<OpenApiExtraDocumentationProperties> listExtraDocProperties) {
    for (final OpenApiExtraDocumentationProperties extraDocProperties : listExtraDocProperties) {
      if (extraDocProperties.getFieldName() != null) {
        final JSONObject propertiesNode = schema.getJSONObject(PROPERTIES_KEY);
        final JSONObject propertyNode = (JSONObject) propertiesNode
                .query(QUERY_JSON_PREFIX + extraDocProperties.getFieldName());

        if (propertyNode != null) {
          this.setFormatAndType(extraDocProperties, propertyNode);
          this.setEnumValues(extraDocProperties, propertyNode);
          this.setItem(extraDocProperties, propertyNode);
        }
      }
    }
  }

  private void setEnumValues(OpenApiExtraDocumentationProperties extraDocProperties,
          JSONObject propertyNode) {
    final List<String> enumsValue = extraDocProperties.getEnumsValue();
    if (enumsValue == null) {
      return;
    }
    propertyNode.put(ENUM_KEY, this.convertObjectAsJsonArray(enumsValue));
  }

  private void setFormatAndType(OpenApiExtraDocumentationProperties extraDocProperties,
          JSONObject propertyNode) {
    final String format = extraDocProperties.getFormat();
    if (format == null) {
      return;
    }
    propertyNode.put(FORMAT_KEY, format);
    this.updateTypeIfNecessary(propertyNode, format);
  }

  private void setItem(OpenApiExtraDocumentationProperties extraDocProperties,
          JSONObject propertyNode) {
    final String item = extraDocProperties.getItem();
    if (item == null) {
      return;
    }
    propertyNode.getJSONObject(ITEMS_KEY).put(TYPE_KEY, OBJECT);
    propertyNode.getJSONObject(ITEMS_KEY).put(REF_KEY, REF_SCHEMAS_VALUE + item);
  }

  private void updateTypeIfNecessary(JSONObject propertyNode, String format) {
    final String type = propertyNode.getString(TYPE_KEY);
    String newType = type;
    switch (format) {
      case FORMAT_INTEGER_INT32:
      case FORMAT_INTEGER_INT64:
        newType = INTEGER;
        break;
      default:
        break;
    }
    if (!newType.equals(type)) {
      propertyNode.put(TYPE_KEY, type);
    }
  }
}
