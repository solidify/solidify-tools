/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PageObject.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import ch.unige.solidify.process.BaseProcess;

public class PageObject extends AttributeObject {
  public class PageProperties {
    private AttributeObject currentPage;
    private AttributeObject sizePage;
    private AttributeObject totalItems;
    private AttributeObject totalPages;

    public AttributeObject getCurrentPage() {
      return this.currentPage;
    }

    public AttributeObject getSizePage() {
      return this.sizePage;
    }

    public AttributeObject getTotalItems() {
      return this.totalItems;
    }

    public AttributeObject getTotalPages() {
      return this.totalPages;
    }

    public void setCurrentPage(AttributeObject currentPage) {
      this.currentPage = currentPage;
    }

    public void setSizePage(AttributeObject sizePage) {
      this.sizePage = sizePage;
    }

    public void setTotalItems(AttributeObject totalItems) {
      this.totalItems = totalItems;
    }

    public void setTotalPages(AttributeObject totalPages) {
      this.totalPages = totalPages;
    }
  }

  private final PageProperties properties = new PageProperties();

  public PageObject() {
    this.setType(BaseProcess.OBJECT);
    this.setDescription("Gives the total pages and the total available items of one resource");
    this.properties.setCurrentPage(new AttributeObject(BaseProcess.NUMBER, "The current page number"));
    this.properties.setSizePage(new AttributeObject(BaseProcess.NUMBER, "The number of item per page"));
    this.properties.setTotalItems(new AttributeObject(BaseProcess.NUMBER, "The total number of items"));
    this.properties.setTotalPages(new AttributeObject(BaseProcess.NUMBER, "The total number of page"));
  }

  public PageProperties getProperties() {
    return this.properties;
  }

}
