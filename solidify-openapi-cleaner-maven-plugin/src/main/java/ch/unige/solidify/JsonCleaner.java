/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - JsonCleaner.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.logging.Log;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ch.unige.solidify.process.BaseProcess;
import ch.unige.solidify.process.OAuth2SecurityProcess;
import ch.unige.solidify.process.PathAddSuffixOnOperationIdProcess;
import ch.unige.solidify.process.PathGetCollectionProcess;
import ch.unige.solidify.process.PathNormalizationProcess;
import ch.unige.solidify.process.PathNotNormalResourceProcess;
import ch.unige.solidify.process.PathRemoveApplicationPrefixOnOperationIdProcess;
import ch.unige.solidify.process.PathRemoveExampleProcess;
import ch.unige.solidify.process.PathUpdateTagsProcess;
import ch.unige.solidify.process.PathWithIdCreationProcess;
import ch.unige.solidify.process.RemoveNullFieldsProcess;
import ch.unige.solidify.process.RemoveOrphanSchemasProcess;
import ch.unige.solidify.process.SchemaArrayTypeProcess;
import ch.unige.solidify.process.SchemaChangeInfoProcess;
import ch.unige.solidify.process.SchemaCollectionAndPageCreationProcess;
import ch.unige.solidify.process.SchemaExtraDocumentationProcess;
import ch.unige.solidify.process.SchemaNameDefinitionProcess;

public class JsonCleaner {
  private JSONObject json;
  private final Map<String, String> migrationNameMap;
  private String stringJsonExtraDocumentation;

  public JsonCleaner() {
    this.migrationNameMap = new HashMap<>();
  }

  public void generate(String outputFilePath) throws IOException {
    this.createDirectory(outputFilePath);
    final Path jsonFile = Paths.get(outputFilePath);
    // Use try-with-resource to get auto-closeable writer instance
    try (BufferedWriter writer = Files.newBufferedWriter(jsonFile)) {
      writer.write(this.getSortedJSON());
    }
  }

  public JSONObject getJson() {
    return this.json;
  }

  public Map<String, String> getMigrationNameMap() {
    return this.migrationNameMap;
  }

  public String getStringJsonExtraDocumentation() {
    return this.stringJsonExtraDocumentation;
  }

  public void load(String inputFilePath, String inputExtraDocumentationFilePath)
          throws IOException {
    this.json = new JSONObject(this.loadJsonFile(inputFilePath));
    if (inputExtraDocumentationFilePath != null) {
      this.stringJsonExtraDocumentation = this.loadJsonFile(inputExtraDocumentationFilePath);
    }
  }

  public void run(String contextPath, List<String> modules, String securityName, String prefix, Log log) {
    // ******************************
    // ** !! Order is important !! **
    // ******************************
    // Normalize Paths
    this.log(log, "Normalizing paths");
    new PathNormalizationProcess(contextPath).process(this.json);
    // Define new schema name with path
    // Change schema name every web services with new name
    // Migrate schemas with new names
    this.log(log, "Defining new schema name");
    new SchemaNameDefinitionProcess(modules, this.migrationNameMap, this.stringJsonExtraDocumentation).process(this.json);
    // Update schema : remove 'oneOf'
    this.log(log, "Updating schemas: removing 'oneOf'");
    new SchemaArrayTypeProcess().process(this.json);
    // Update schemas with extra info
    this.log(log, "Updating schemas: adding extry info");
    new SchemaExtraDocumentationProcess(this.stringJsonExtraDocumentation).process(this.json);
    // Add creation & lastUpdate fields for each schemas
    // Add changInfo info in schemas
    this.log(log, "Adding creation & last update info");
    new SchemaChangeInfoProcess().process(this.json);
    // Add collection & page info in schemas
    this.log(log, "Adding collection & page info");
    new SchemaCollectionAndPageCreationProcess().process(this.json);
    // Update operationId
    this.log(log, "Updating 'operationId'");
    new PathRemoveApplicationPrefixOnOperationIdProcess().process(this.json);
    // Remove examples nodes in responses and requestBody
    this.log(log, "Removing example nodes");
    new PathRemoveExampleProcess().process(this.json);
    // Add new path with id (for GET, PATCH, DELETE)
    this.log(log, "Adding new paths for GET, PATH & DELETE");
    new PathWithIdCreationProcess().process(this.json);
    // Add to existing path with POST verb the other verb GET that return collection
    this.log(log, "Updating paths with POST/GET");
    new PathGetCollectionProcess().process(this.json);
    // Manage resource exceptions (not normal resource)
    this.log(log, "Updating paths for not normal resources");
    new PathNotNormalResourceProcess(this.stringJsonExtraDocumentation).process(this.json);
    // Add suffix (/get, /post, /get-by-id, /delete-by-id, /patch-by-id) to operation id for each
    // verb
    this.log(log, "Adding suffixes");
    new PathAddSuffixOnOperationIdProcess().process(this.json);
    // Update tags for each web services
    this.log(log, "Updating tags");
    new PathUpdateTagsProcess().process(this.json);
    // Remove 'null' fields
    this.log(log, "Removing 'null' fields");
    new RemoveNullFieldsProcess().process(this.json);
    // Remove orphan schema
    this.log(log, "Removing orphan schemas");
    new RemoveOrphanSchemasProcess(prefix).process(this.json);
    // Add security info for OAuth2
    this.log(log, "Adding security");
    new OAuth2SecurityProcess(securityName).process(this.json);
  }

  public void setJson(JSONObject json) {
    this.json = json;
  }

  public void setStringJsonExtraDocumentation(String stringJsonExtraDocumentation) {
    this.stringJsonExtraDocumentation = stringJsonExtraDocumentation;
  }

  private void createDirectory(String outputFilePath) {
    final File directory = new File(this.getDirectoryPath(outputFilePath));
    if (!directory.exists()) {
      directory.mkdirs();
    }
  }

  private String getDirectoryPath(String outputFilePath) {
    if (outputFilePath.endsWith(BaseProcess.URL_SEPARATOR)) {
      return outputFilePath;
    }
    final int lastIndexOfSeparator = outputFilePath.lastIndexOf(BaseProcess.URL_SEPARATOR);
    return outputFilePath.substring(0, lastIndexOfSeparator);
  }

  private String getSortedJSON() throws IOException {
    final String jsonString = this.json.toString(2);
    final ObjectMapper mapper = new ObjectMapper().copy()
            .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
            .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
            .configure(SerializationFeature.INDENT_OUTPUT, true);
    final Object sortedJson = mapper.readValue(jsonString, Object.class);
    return mapper.writeValueAsString(sortedJson);
  }

  private String loadJsonFile(String inputFilePath) throws IOException {
    final Path jsonFile = Paths.get(inputFilePath);
    return new String(Files.readAllBytes(jsonFile), StandardCharsets.UTF_8);
  }

  @SuppressWarnings("squid:S106")
  private void log(Log log, String message) {
    if (log == null) {
      System.out.println(message);
    } else {
      log.info(message);
    }
  }
}
