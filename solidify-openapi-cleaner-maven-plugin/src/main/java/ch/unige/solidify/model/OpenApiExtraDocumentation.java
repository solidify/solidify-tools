/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - OpenApiExtraDocumentation.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.util.ArrayList;
import java.util.List;

public class OpenApiExtraDocumentation {
  private String path;
  private String schema;
  private boolean normalResource;
  private List<OpenApiExtraDocumentationProperties> properties;

  public OpenApiExtraDocumentation() {
  }

  public OpenApiExtraDocumentation(String path) {
    this.path = path;
    this.schema = this.getSchema(path);
    this.properties = new ArrayList<>();
  }

  public String getPath() {
    return this.path;
  }

  public List<OpenApiExtraDocumentationProperties> getProperties() {
    return this.properties;
  }

  public String getSchema() {
    return this.schema;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public void setProperties(List<OpenApiExtraDocumentationProperties> properties) {
    this.properties = properties;
  }

  public void setSchema(String schema) {
    this.schema = schema;
  }

  private String getSchema(String path) {
    if (path.lastIndexOf('/') == -1) {
      return null;
    }
    return path.substring(path.lastIndexOf('/') + 1);
  }

  public boolean isNormalResource() {
    return this.normalResource;
  }

  public void setNormalResource(boolean normalResource) {
    this.normalResource = normalResource;
  }

}
