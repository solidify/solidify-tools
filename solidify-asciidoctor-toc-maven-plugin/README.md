# Purpose

This maven plugin allow to extract TOC from generated HTML asciidoctor document by :
- normalize HTML document
- remove content
- keep TOC only
