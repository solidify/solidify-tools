/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PathUpdateTagsProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Update tags with module name
 */
public class PathUpdateTagsProcess extends BaseProcess {

  @Override
  public void process(JSONObject json) {
    for (final Iterator<String> i = this.getPathsNode(json).keys(); i.hasNext();) {
      final String pathName = i.next();
      if (pathName.equals("/")) {
        continue;
      }
      final JSONObject path = this.getPathsNode(json).getJSONObject(pathName);
      final String[] paths = pathName.split("\\/");
      for (final Iterator<String> j = path.keys(); j.hasNext();) {
        final String wsName = j.next();
        final JSONArray tags = path.getJSONObject(wsName).getJSONArray("tags");
        if (paths.length >= 2) {
          // Remove application
          tags.remove(0);
          // Add module
          tags.put(paths[1]);
        }
      }
    }
  }

}
