/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - JsonCleanerTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ch.unige.solidify.JsonCleaner;

@ExtendWith(MockitoExtension.class)
public class JsonCleanerTest {
  private final String CONTEXT_PATH = "/dlcm";
  private final String OUTPUT_FOLDER = "target/output/insideSubDir/";
  private final String FILE_OPEN_API_EXTRA_DOCUMENTATION_TO_TEST = "openapi-extra-documentation";
  private final String FILE_OPEN_API_TO_TEST = "openapi-3.0";
  private final String JSON = ".json";
  private final JsonCleaner jsonCleaner = new JsonCleaner();
  private final List<String> MODULE_LIST = Arrays
          .asList("/admin", "/access", "/archival-storage", "/data-mgmt", "/ingest", "/preingest", "/resource-srv",
                  "/preservation-planning");
  private final String SECURITY_NAME = "dlcm-auth";
  private final String PREFIX = "dlcm";

  private final String SRC_TEST_RESOURCES = "src/test/resources/";

  @Test
  public void sortJSON() throws IOException {
    this.readFile();
    final String jsonString = this.jsonCleaner.getJson().toString();

    final ObjectMapper mapper = new ObjectMapper().copy()
            .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
            .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
            .configure(SerializationFeature.INDENT_OUTPUT, true);
    final Object json = mapper.readValue(jsonString, Object.class);
    this.saveData(mapper.writeValueAsString(json));
  }

  @Test
  public void testReadFile() {
    this.readFile();

    assertNotNull(this.jsonCleaner.getJson());
  }

  @Test
  public void testRun() {
    this.readFile();

    final JSONObject jsonBeforeCleanup = this.copyJsonObject(this.jsonCleaner.getJson());
    this.jsonCleaner.run(this.CONTEXT_PATH, this.MODULE_LIST, this.SECURITY_NAME, this.PREFIX, null);
    final JSONObject jsonAfterCleanup = this.jsonCleaner.getJson();

    new AssertGeneralProcessImpl(jsonBeforeCleanup).process(jsonAfterCleanup);
    new AssertSchemasProcessImpl(jsonBeforeCleanup, this.jsonCleaner.getMigrationNameMap().keySet().size()).process(jsonAfterCleanup);
    new AssertPathsProcessImpl(jsonBeforeCleanup, this.jsonCleaner.getStringJsonExtraDocumentation()).process(jsonAfterCleanup);

    try {
      this.jsonCleaner.generate(this.OUTPUT_FOLDER + this.FILE_OPEN_API_TO_TEST + "-cleaned.json");
    } catch (final IOException e) {
      fail("IO exception: " + e.getMessage());
      e.printStackTrace();
    }
  }

  private JSONObject copyJsonObject(JSONObject jsonObjectToCopy) {
    return new JSONObject(jsonObjectToCopy.toString());
  }

  private void readFile() {
    try {
      this.jsonCleaner.load(this.SRC_TEST_RESOURCES + this.FILE_OPEN_API_TO_TEST + this.JSON,
              this.SRC_TEST_RESOURCES + this.FILE_OPEN_API_EXTRA_DOCUMENTATION_TO_TEST + this.JSON);
    } catch (final IOException e) {
      fail("Test file not found: " + e.getMessage());
      e.printStackTrace();
    }
  }

  private void saveData(String string) throws IOException {
    // Get the file reference
    final Path json = Paths.get(this.OUTPUT_FOLDER + this.FILE_OPEN_API_TO_TEST + "-sorted.json");

    // Use try-with-resource to get auto-closeable writer instance
    try (BufferedWriter writer = Files.newBufferedWriter(json)) {
      writer.write(string);
    }
  }
}
