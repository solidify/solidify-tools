/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - SchemaNameDefinitionProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import ch.unige.solidify.model.OpenApiExtraDocumentation;

/**
 * Define old & new name relationship with webservice
 */
public class SchemaNameDefinitionProcess extends BaseProcess {
  private final List<OpenApiExtraDocumentation> listOpenApiExtraDocumentation;
  private final Map<String, String> migrationNameMap;
  private final ArrayList<String> moduleList = new ArrayList<>();

  public SchemaNameDefinitionProcess(List<String> modules, Map<String, String> migrationNameMap,
          String jsonExtraDocumentation) {
    this.setModules(modules);
    this.migrationNameMap = migrationNameMap;
    this.listOpenApiExtraDocumentation = this.loadOpenApiExtraDocumentation(jsonExtraDocumentation);
  }

  @Override
  public void process(JSONObject json) {
    // Change schema name for application
    final JSONObject applicaionJson = this.getPathsNode(json).getJSONObject("/");
    this.updateSchemaInPath(applicaionJson, "application");
    // Change schema name every modules
    for (final String module : this.getModules()) {
      final JSONObject moduleJson = this.getPathsNode(json).getJSONObject(module);
      this.updateSchemaInPath(moduleJson, module.substring(1));
    }
    // Change schema name every web services
    for (final OpenApiExtraDocumentation resource : this.listOpenApiExtraDocumentation) {
      final JSONObject resourceJson = this.getPathsNode(json).getJSONObject(resource.getPath());
      this.updateSchemaInPath(resourceJson, resource.getSchema());
    }
    // Migrate schemas with new name
    this.migrateSchemaNames(json);
  }

  private ArrayList<String> getModules() {
    return this.moduleList;
  }

  private void migrateSchemaNames(JSONObject json) {
    final JSONObject schemas = this.getSchemasNode(json);
    for (final Map.Entry<String, String> names : this.migrationNameMap.entrySet()) {
      // Get JSON values
      final Object obj = schemas.get(names.getKey());
      // Add JSON values with new name
      schemas.put(names.getValue(), obj);
      // Remove JSON values with old name
      schemas.remove(names.getKey());
    }
  }

  private void setModules(List<String> modules) {
    for (final String module : modules) {
      if (module.startsWith("/")) {
        this.moduleList.add(module);
      } else {
        this.moduleList.add("/" + module);
      }

    }
  }

  private void updateSchemaInPath(JSONObject resource, String newSchemaName) {
    // Iterate for each web service
    for (final String httpVerb : this.getHttpVerb()) {
      if (!resource.isNull(httpVerb)) {
        final JSONObject ws = resource.getJSONObject(httpVerb);

        final String originalModelName = ws.getJSONObject(RESPONSES_KEY)
                .getJSONObject(this.getResponse(httpVerb)).getJSONObject(CONTENT_KEY)
                .getJSONObject(CONTENT_TYPE_KEY).getJSONObject(SCHEMA_KEY).getString(REF_KEY)
                .substring(REF_SCHEMAS_VALUE.length());
        this.migrationNameMap.put(originalModelName, newSchemaName);
        this.updateSchemaReferences(ws, httpVerb, newSchemaName);
      }
    }
  }

  private void updateSchemaReferences(JSONObject ws, String httpVerb, String newSchemaName) {
    if (!ws.isNull("requestBody")) {
      ws.getJSONObject("requestBody").getJSONObject(CONTENT_KEY).getJSONObject("application/json")
              .getJSONObject(SCHEMA_KEY).put(REF_KEY, REF_SCHEMAS_VALUE + newSchemaName);
    }
    ws.getJSONObject("responses").getJSONObject(this.getResponse(httpVerb)).getJSONObject(CONTENT_KEY)
            .getJSONObject(CONTENT_TYPE_KEY).getJSONObject(SCHEMA_KEY)
            .put(REF_KEY, REF_SCHEMAS_VALUE + newSchemaName);
  }

}
