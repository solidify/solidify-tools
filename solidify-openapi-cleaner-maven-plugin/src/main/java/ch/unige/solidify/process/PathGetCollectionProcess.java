/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PathGetCollectionProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;

import org.json.JSONObject;

import ch.unige.solidify.model.QueryParametersObject;

/**
 * Add to existing path with POST verb the other verb GET that return collection (reference the
 * schema collection and add query parameters)
 */
public class PathGetCollectionProcess extends BaseProcess {

  @Override
  public void process(JSONObject json) {
    for (final Iterator<String> i = this.getPathsNode(json).keys(); i.hasNext();) {
      final String pathName = i.next();
      final JSONObject path = this.getPathsNode(json).getJSONObject(pathName);

      // Check if resource collection web service only
      // not application, not module, not resource-by-id
      if (this.countCharacter(pathName, '/') != 2) {
        continue;
      }

      if (!this.isReadOnlyResource(path)) {
        JSONObject postPath = (JSONObject) path.query(QUERY_JSON_PREFIX + POST_PATH);
        // Update response to 200
        postPath = this.updateResponse(postPath, "201", "200");
        // Create collection get
        this.createGetPath(path, postPath);
      } else {
        final JSONObject getPath = (JSONObject) path.query(QUERY_JSON_PREFIX + GET_PATH);
        // Create collection get
        this.createGetPath(path, getPath);
      }
    }
  }

  private void addQueryParameters(JSONObject getPath) {
    final JSONObject queryParameters = this.convertObjectAsJsonObject(new QueryParametersObject());
    getPath.put(PARAMETERS_KEY, queryParameters.getJSONArray(PARAMETERS_KEY));
  }

  private void createGetPath(JSONObject path, JSONObject postPath) {
    final JSONObject getPath = this.generateGetPathFromPostPath(postPath);
    path.put(GET_PATH, getPath);
  }

  private JSONObject generateGetPathFromPostPath(JSONObject postPath) {
    final JSONObject getPath = this.deepCopy(postPath);
    getPath.remove(REQUEST_BODY_KEY);
    this.updateSchemaRefToCollection(getPath);
    this.addQueryParameters(getPath);
    return getPath;
  }

  private JSONObject getSchemaNode(JSONObject getPath) {
    final JSONObject responses = getPath.getJSONObject(RESPONSES_KEY);
    final JSONObject response200 = this.getFirstChild(responses);
    final JSONObject content = response200.getJSONObject(CONTENT_KEY);
    final JSONObject applicationHalJson = this.getFirstChild(content);
    return applicationHalJson.getJSONObject(SCHEMA_KEY);
  }

  private void updateSchemaRefToCollection(JSONObject getPath) {
    final JSONObject schema = this.getSchemaNode(getPath);
    schema.put(REF_KEY, REF_SCHEMAS_VALUE + COLLECTION_NAME);
  }
}
