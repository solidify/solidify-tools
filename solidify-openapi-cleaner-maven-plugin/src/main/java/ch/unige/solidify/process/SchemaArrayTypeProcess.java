/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - SchemaArrayTypeProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;

import org.json.JSONObject;

/**
 * Force type of all array into string
 */
public class SchemaArrayTypeProcess extends BaseProcess {

  @Override
  public void process(JSONObject json) {
    // Update schema : remove 'oneOf'
    this.updateSchemasArrayTypeRecursively(this.getSchemasNode(json));
  }

  private void updateOneOfSchemas(JSONObject obj, String key) {
    obj.remove(key);
    obj.put(TYPE_KEY, STRING);
  }

  private void updateSchemasArrayTypeRecursively(JSONObject obj) {
    for (final Iterator<String> i = obj.keys(); i.hasNext();) {
      final String key = i.next();

      if (key.equals(ONE_OF)) {
        this.updateOneOfSchemas(obj, key);
        break;
      }
      if (obj.get(key).getClass() == JSONObject.class) {
        this.updateSchemasArrayTypeRecursively(obj.getJSONObject(key));
      }
    }
  }
}
