/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PathAddSuffixOnOperationIdProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;

import org.json.JSONObject;

/**
 * Add suffix (/get, /post, /get-by-id, /delete-by-id, /patch-by-id) to operation id for each verb
 * (GET, POST, DELETE, PATCH)
 */
public class PathAddSuffixOnOperationIdProcess extends BaseProcess {

  @Override
  public void process(JSONObject json) {
    for (final Iterator<String> i = this.getPathsNode(json).keys(); i.hasNext();) {
      final String pathName = i.next();
      final JSONObject path = this.getPathsNode(json).getJSONObject(pathName);

      final boolean isWithId = this.determinePathWithId(pathName);
      this.iterateOverPathVerb(path, isWithId);
    }
  }

  private void applySuffixToOperationId(JSONObject verb, String suffix) {
    final String operationId = verb.getString(OPERATION_ID_KEY);
    verb.put(OPERATION_ID_KEY, operationId + suffix);
  }

  private boolean determinePathWithId(String pathName) {
    return pathName.endsWith(RES_ID_URL_PARAM);
  }

  private String getSuffix(String verbName, boolean isWithId) {
    return URL_SEPARATOR + verbName + (isWithId ? BY_ID_SUFFIX : EMPTY);
  }

  private void iterateOverPathVerb(JSONObject path, boolean isWithId) {
    for (final Iterator<String> i = path.keys(); i.hasNext();) {
      final String verbName = i.next();
      final JSONObject verb = path.getJSONObject(verbName);

      final String suffix = this.getSuffix(verbName, isWithId);
      this.applySuffixToOperationId(verb, suffix);
    }
  }
}
