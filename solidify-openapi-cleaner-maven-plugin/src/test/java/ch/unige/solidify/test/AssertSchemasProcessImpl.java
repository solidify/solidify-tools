/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - AssertSchemasProcessImpl.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.json.JSONObject;

/**
 * Assert after execution of plugin
 */
public class AssertSchemasProcessImpl extends AssertBaseProcess {
  private int migratedSchemas;

  public AssertSchemasProcessImpl(JSONObject jsonBefore, int migratedSchemas) {
    super(jsonBefore);
    this.migratedSchemas = migratedSchemas;
  }

  @Override
  public void process(JSONObject jsonAfter) {
    final int additionalSchemas = 3; // Collection + ChangeInfo + Page

    final JSONObject schemaAfter = this.getSchemasNode(jsonAfter);

    // Test objects names
    for (String name : schemaAfter.keySet()) {
      assertFalse(name.startsWith("dlcm-"), "Wrong schema name '" + name + "'");
    }
    // Test number of objects
    final int expectedNumberOfSchema = this.migratedSchemas + additionalSchemas;
    assertEquals(expectedNumberOfSchema, this.getSchemasNode(jsonAfter).length());
  }

}
