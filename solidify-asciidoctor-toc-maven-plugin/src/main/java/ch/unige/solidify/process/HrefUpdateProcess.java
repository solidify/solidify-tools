/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify asciidoctor TOC Extractor - HrefUpdateProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.io.Serializable;

import javax.xml.bind.JAXBElement;

import ch.unige.solidify.model.xml.xhtml.v1.XhtmlAType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlHtmlType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlLiType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlUlType;

public class HrefUpdateProcess extends BaseProcess {
  private final String fileName;

  public HrefUpdateProcess(String fileName) {
    this.fileName = fileName;
  }

  @Override
  public JAXBElement<XhtmlUlType> process(JAXBElement<XhtmlHtmlType> html, JAXBElement<XhtmlUlType> toc) {
    this.updateLink(toc.getValue());
    return toc;
  }

  private void updateLink(XhtmlUlType toc) {
    for (final XhtmlLiType li : toc.getLi()) {
      for (final Serializable item : li.getContent()) {
        if (item instanceof JAXBElement<?>) {
          final Object obj = ((JAXBElement<?>) item).getValue();
          if (obj instanceof XhtmlUlType) {
            this.updateLink((XhtmlUlType) obj);
          } else if (obj instanceof XhtmlAType) {
            final XhtmlAType link = (XhtmlAType) obj;
            link.setHref(this.fileName + link.getHref());
            link.setTarget("_blank");
          }
        }
      }
    }
  }
}
