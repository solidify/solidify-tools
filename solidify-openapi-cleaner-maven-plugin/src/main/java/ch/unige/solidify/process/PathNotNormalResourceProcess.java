/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PathNotNormalResourceProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;

import ch.unige.solidify.model.OpenApiExtraDocumentation;

/**
 * Force type of all array into string
 */
public class PathNotNormalResourceProcess extends BaseProcess {

  private final List<OpenApiExtraDocumentation> notNormalResourceList;

  public PathNotNormalResourceProcess(String jsonExtraDocumentation) {
    this.notNormalResourceList = this.loadOpenApiExtraDocumentation(jsonExtraDocumentation).stream()
            .filter(field -> !field.isNormalResource()).collect(Collectors.toList());
  }

  @Override
  public void process(JSONObject json) {
    // Not normal resource ==> Only get
    for (final OpenApiExtraDocumentation notNormalResource : this.notNormalResourceList) {
      JSONObject paths = this.getPathsNode(json);
      // Update
      JSONObject getPath = paths.getJSONObject(notNormalResource.getPath());
      for (final Iterator<String> f = getPath.keys(); f.hasNext();) {
        final String fieldName = f.next();
        // Update Get
        if (fieldName.equals(GET_PATH)) {
          // Update schema
          getPath.getJSONObject(GET_PATH)
                  .getJSONObject(RESPONSES_KEY)
                  .getJSONObject(STATUS_200_KEY)
                  .getJSONObject(CONTENT_KEY)
                  .getJSONObject(CONTENT_TYPE_KEY)
                  .getJSONObject(SCHEMA_KEY)
                  .put(REF_KEY, REF_SCHEMAS_VALUE + notNormalResource.getSchema());
          // Remove parameters
          getPath.getJSONObject(GET_PATH)
                  .remove(PARAMETERS_KEY);
        } else {
          // Remove others verbs
          getPath.remove(fieldName);
        }
      }
      // Delete get with resId
      paths.remove(notNormalResource.getPath() + URL_SEPARATOR + RES_ID_URL_PARAM);
    }
  }

}
