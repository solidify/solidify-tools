/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PathNormalizationProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

/**
 * Define old & new name relationship with webservice
 */
public class PathNormalizationProcess extends BaseProcess {

  private final String contextPath;

  public PathNormalizationProcess(String contextPath) {
    this.contextPath = contextPath;
  }

  @Override
  public void process(JSONObject json) {
    final JSONObject pathsJson = this.getPathsNode(json);
    final Map<String, Object> newList = new HashMap<>();

    // Rename & save path object
    for (final String path : pathsJson.keySet()) {
      final JSONObject pathObj = pathsJson.getJSONObject(path);

      // Remove prefix of context path
      String newPath;
      if (path.equals(this.contextPath)) {
        newPath = "/";
      } else {
        newPath = path.substring(this.contextPath.length());
      }
      final long count = this.countCharacter(path, '/');
      if (count == 4) {
        newPath = path.substring(this.contextPath.length(), path.lastIndexOf('/'));
      }
      newList.put(newPath, pathObj);
    }

    // Update paths object
    this.migrateNodes(json, PATHS_KEY, newList);
  }

}
