/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PathRemoveApplicationPrefixOnOperationIdProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;

import org.json.JSONObject;

/**
 * Add to path with POST verb the other verb (GET, GET_BY_ID, PATCH, DELETE)
 */
public class PathRemoveApplicationPrefixOnOperationIdProcess extends BaseProcess {

  @Override
  public void process(JSONObject json) {
    for (final Iterator<String> i = this.getPathsNode(json).keys(); i.hasNext();) {
      final String pathName = i.next();
      final JSONObject path = this.getPathsNode(json).getJSONObject(pathName);

      final JSONObject verb = this.getFirstChild(path);
      if (verb == null) {
        continue;
      }

      this.applyNewOperationIdName(verb);
    }
  }

  private void applyNewOperationIdName(JSONObject verb) {
    String operationId = verb.getString(OPERATION_ID_KEY);
    operationId = this.removeApplicationOperationIdPrefix(operationId);
    verb.put(OPERATION_ID_KEY, operationId);
  }

  private String removeApplicationOperationIdPrefix(String operationId) {
    if (operationId.startsWith(APPLICATION_OPERATION_ID_PREFIX)) {
      operationId = operationId.substring(APPLICATION_OPERATION_ID_PREFIX.length());
    }
    return operationId;
  }
}
