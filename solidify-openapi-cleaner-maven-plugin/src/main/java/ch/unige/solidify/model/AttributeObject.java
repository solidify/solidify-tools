/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - AttributeObject.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import ch.unige.solidify.process.BaseProcess;

public class AttributeObject {
  private String description;
  private String format;
  private String type;
  private String $ref;

  public AttributeObject(String type) {
    this.type = type;
  }

  public AttributeObject(String type, String description) {
    if (type.startsWith(BaseProcess.REF_SCHEMAS_VALUE)) {
      this.type = BaseProcess.OBJECT;
      this.$ref = type;
    } else {
      this.type = type;
    }
    this.description = description;
  }

  public AttributeObject(String type, String description, String format) {
    this(type, description);
    this.format = format;
  }

  protected AttributeObject() {
  }

  public String getDescription() {
    return this.description;
  }

  public String getFormat() {
    return this.format;
  }

  public String getType() {
    return this.type;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String get$ref() {
    return this.$ref;
  }

  public void set$ref(String $ref) {
    this.$ref = $ref;
  }

}
