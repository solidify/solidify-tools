/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - BaseProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.model.JsonCleanerException;
import ch.unige.solidify.model.OpenApiExtraDocumentation;

@SuppressWarnings("squid:S2068")
public abstract class BaseProcess implements IProcess {
  public static final String URL_SEPARATOR = "/";
  public static final String APPLICATION_OPERATION_ID_PREFIX = "application/";
  public static final String ATTRIBUTE_CREATION = "creation";
  public static final String ATTRIBUTE_LAST_UPDATE = "lastUpdate";
  public static final String BOOLEAN = "boolean";
  public static final String BY_ID_SUFFIX = "-by-id";
  public static final String CHANGE_INFO_NAME = "change-info";
  public static final String COLLECTION_NAME = "collection";
  public static final String COMPONENTS_KEY = "components";
  public static final String CONTENT_KEY = "content";
  public static final String DELETE_PATH = "delete";
  public static final String EMPTY = "";
  public static final String ENUM_KEY = "enum";
  public static final String EXAMPLES_KEY = "examples";
  public static final String FILTERS = "filters";
  public static final String FORM = "form";
  public static final String FORMAT_INTEGER_INT32 = "int32";
  public static final String FORMAT_INTEGER_INT64 = "int64";
  public static final String FORMAT_KEY = "format";
  public static final String FORMAT_NUMBER_DOUBLE = "double";
  public static final String FORMAT_NUMBER_FLOAT = "float";
  public static final String FORMAT_STRING_BINARY = "binary";
  public static final String FORMAT_STRING_BYTE = "byte";
  public static final String FORMAT_STRING_DATE = "date";
  public static final String FORMAT_STRING_DATE_TIME = "date-time";
  public static final String FORMAT_STRING_PASSWORD = "password";
  public static final String GET_PATH = "get";
  public static final String INTEGER = "integer";
  public static final String ITEMS_KEY = "items";
  public static final String NUMBER = "number";
  public static final String OBJECT = "object";
  public static final String ONE_OF = "oneOf";
  public static final String OPERATION_ID_KEY = "operationId";
  public static final String PAGE = "page";
  public static final String PAGE_NAME = "page";
  public static final String PARAMETERS_KEY = "parameters";
  public static final String PATCH_PATH = "patch";
  public static final String PATHS_KEY = "paths";
  public static final String POST_PATH = "post";
  public static final String PROPERTIES_KEY = "properties";
  public static final String QUERY = "query";
  public static final String QUERY_JSON_PREFIX = URL_SEPARATOR;
  public static final String REF_KEY = "$ref";
  public static final String REF_SCHEMAS_VALUE = "#/components/schemas/";
  public static final String REGEX_SUFFIX_HASH = "(-)?[0-9]{2,}";
  public static final String REQUEST_BODY_KEY = "requestBody";
  public static final String RES_ID_URL_PARAM = "{resId}"; // FOR DELETE, PATCH, GETBYID
  public static final String RESPONSES_KEY = "responses";
  public static final String ROOT_PATH = URL_SEPARATOR;
  public static final String SCHEMA_KEY = "schema";
  public static final String SCHEMAS_KEY = "schemas";
  public static final String SIZE = "size";
  public static final String SORT = "sort";
  public static final String STRING = "string";
  public static final String STRING_EMPTY = "";
  public static final String SUFFIX_EXAMPLE_MODEL = "_example";
  public static final String TYPE_KEY = "type";
  public static final String STATUS_200_KEY = "200";
  public static final String STATUS_201_KEY = "201";
  public static final String CONTENT_TYPE_KEY = "application/hal+json";

  public BaseProcess() {
  }

  protected JSONArray convertObjectAsJsonArray(Object object) {
    try {
      return new JSONArray(this.toJson(object));
    } catch (final JSONException e) {
      throw new JsonCleanerException(e.getMessage(), e);
    }
  }

  protected JSONObject convertObjectAsJsonObject(Object object) {
    try {
      return new JSONObject(this.toJson(object));
    } catch (final JSONException e) {
      throw new JsonCleanerException(e.getMessage(), e);
    }
  }

  protected long countCharacter(String string, char c) {
    return string.chars().filter(ch -> ch == c).count();
  }

  protected JSONObject deepCopy(JSONObject original) {
    return new JSONObject(original.toString());
  }

  protected JSONObject getComponentsNode(JSONObject json) {
    return json.getJSONObject(COMPONENTS_KEY);
  }

  protected JSONObject getFirstChild(JSONObject parent) {
    if (parent.keys().hasNext()) {
      final String firstChildKey = parent.keys().next();
      return parent.getJSONObject(firstChildKey);
    }
    return null;
  }

  protected List<String> getHttpVerb() {
    final List<String> crudList = new ArrayList<>();
    crudList.add(POST_PATH);
    crudList.add(GET_PATH);
    return crudList;
  }

  protected JSONObject getPathsNode(JSONObject json) {
    return json.getJSONObject(PATHS_KEY);
  }

  protected String getResponse(String httpVerb) {
    if (httpVerb.equals(POST_PATH)) {
      return STATUS_201_KEY;
    }
    return STATUS_200_KEY;
  }

  protected JSONObject getSchemasNode(JSONObject json) {
    return this.getComponentsNode(json).getJSONObject(SCHEMAS_KEY);
  }

  protected boolean isReadOnlyResource(JSONObject path) {
    return (path.query(QUERY_JSON_PREFIX + POST_PATH) == null);
  }

  protected List<OpenApiExtraDocumentation> loadOpenApiExtraDocumentation(
          String jsonExtraDocumentation) {
    final ObjectMapper mapper = new ObjectMapper();
    final JavaType listType = mapper.getTypeFactory().constructParametricType(List.class,
            OpenApiExtraDocumentation.class);
    try {
      return mapper.readValue(jsonExtraDocumentation, listType);
    } catch (final IOException e) {
      throw new JsonCleanerException(e.getMessage(), e);
    }
  }

  protected JSONObject mergeJsonObject(JSONObject o1, JSONObject o2) {
    final JSONObject merged = new JSONObject(o1, JSONObject.getNames(o1));
    for (final String key : JSONObject.getNames(o2)) {
      merged.put(key, o2.get(key));
    }
    return merged;
  }

  protected void migrateNodes(JSONObject object, String key, Map<String, Object> map) {
    object.remove(key);
    final JSONObject newDef = new JSONObject(map);
    object.put(key, newDef);
  }

  protected String toJson(Object object) {
    try {
      return new ObjectMapper().writeValueAsString(object);
    } catch (final JsonProcessingException e) {
      throw new JsonCleanerException(e.getMessage(), e);
    }
  }

  protected JSONObject updateResponse(JSONObject httpPath, String fromValue, String toValue) {
    if (!httpPath.getJSONObject(RESPONSES_KEY).isNull(fromValue)) {
      final JSONObject newHttpPath = this.deepCopy(httpPath);
      final JSONObject response = newHttpPath.getJSONObject(RESPONSES_KEY).getJSONObject(fromValue);
      response.put("description", toValue);
      newHttpPath.getJSONObject(RESPONSES_KEY).remove(fromValue);
      newHttpPath.getJSONObject(RESPONSES_KEY).put(toValue, response);
      return newHttpPath;
    }
    return httpPath;
  }
}
