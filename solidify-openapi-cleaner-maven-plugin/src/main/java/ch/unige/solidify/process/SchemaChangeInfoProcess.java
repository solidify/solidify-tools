/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - SchemaChangeInfoProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;

import org.json.JSONObject;

import ch.unige.solidify.model.ChangeInfoObject;

/**
 * Force type of lastUpdate and creation attribute to ChangeInfo
 */
public class SchemaChangeInfoProcess extends BaseProcess {
  private final JSONObject changeInfoJsonObject;

  public SchemaChangeInfoProcess() {
    this.changeInfoJsonObject = this.getChangeInfoJsonObject();
  }

  @Override
  public void process(JSONObject json) {
    // Add creation & lastUpdate field
    this.iterateOverSchema(json);
    // Add changInfo info in schemas
    this.addChangeInfo(json);
  }

  private void addChangeInfo(JSONObject json) {
    this.getSchemasNode(json).put(CHANGE_INFO_NAME, this.changeInfoJsonObject);
  }

  private String getChangeInfoJson() {
    return this.toJson(new ChangeInfoObject());
  }

  private JSONObject getChangeInfoJsonObject() {
    return new JSONObject(this.getChangeInfoJson());
  }

  private void iterateOverSchema(JSONObject json) {
    for (final Iterator<String> i = this.getSchemasNode(json).keys(); i.hasNext();) {
      final String schemaName = i.next();
      final JSONObject schema = this.getSchemasNode(json).getJSONObject(schemaName);
      final JSONObject propertiesNode = (JSONObject) schema.query(QUERY_JSON_PREFIX + PROPERTIES_KEY);
      if (propertiesNode == null) {
        continue;
      }
      this.setChangeInfoType(propertiesNode, ATTRIBUTE_LAST_UPDATE);
      this.setChangeInfoType(propertiesNode, ATTRIBUTE_CREATION);
    }
  }

  private void setChangeInfoType(JSONObject propertiesNode, String key) {
    final JSONObject node = (JSONObject) propertiesNode.query(QUERY_JSON_PREFIX + key);
    if (node == null) {
      return;
    }
    node.put(REF_KEY, REF_SCHEMAS_VALUE + CHANGE_INFO_NAME);
  }
}
