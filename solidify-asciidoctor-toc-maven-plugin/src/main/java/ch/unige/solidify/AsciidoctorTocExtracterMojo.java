/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify asciidoctor TOC Extractor - AsciidoctorTocExtracterMojo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import ch.unige.solidify.process.TocExtractionException;

@Mojo(name = "extract-toc", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class AsciidoctorTocExtracterMojo extends AbstractMojo {

  @Parameter(property = "inputFile", required = true)
  private String inputFile;

  @Parameter(property = "skip", required = false, defaultValue = "false")
  private boolean skip;

  @Override
  @SuppressWarnings("squid:S1148")
  public void execute() throws MojoExecutionException {
    if (this.skip) {
      this.getLog().info("Skipping asciidoctor TOC extraction");
      return;
    }
    final AsciidoctorTocExtracter tocExtractor = new AsciidoctorTocExtracter();
    try {
      this.getLog().info("Check input file: " + this.inputFile);
      tocExtractor.load(this.inputFile);
      this.getLog().info("Process asciidoctor TOC extraction");
      tocExtractor.run(this.getLog());
      tocExtractor.generate();
      this.getLog().info("Successfully HTML TOC generated");
    } catch (final FileNotFoundException e) {
      this.getLog().info("File not found: " + e.getMessage());
      throw new MojoExecutionException(e.getMessage(), e);
    } catch (final IOException e) {
      this.getLog().info("IO Exception: " + e.getMessage());
      throw new MojoExecutionException(e.getMessage(), e);
    } catch (final JAXBException e) {
      this.getLog().info("JAXB Exception: " + e.getMessage());
      throw new MojoExecutionException(e.getMessage(), e);
    } catch (final TocExtractionException e) {
      this.getLog().info("TOC Extraction Exception: " + e.getMessage());
      throw new MojoExecutionException(e.getMessage(), e);
    }

  }
}
