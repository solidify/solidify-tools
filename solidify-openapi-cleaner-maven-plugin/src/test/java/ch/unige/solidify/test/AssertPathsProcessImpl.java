/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - AssertPathsProcessImpl.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Iterator;

import org.json.JSONObject;

/**
 * Assert after execution of plugin
 */
public class AssertPathsProcessImpl extends AssertBaseProcess {

  private String jsonExtraDocumentation;

  public AssertPathsProcessImpl(JSONObject jsonBefore, String jsonExtraDocumentation) {
    super(jsonBefore);
    this.jsonExtraDocumentation = jsonExtraDocumentation;
  }

  @Override
  public void process(JSONObject jsonAfter) {
    final JSONObject pathsJson = this.getPathsNode(this.jsonBefore);
    final int numberOfPost = this.countResourcePathsChild(pathsJson, "post");
    final int numberOfGet = this.countResourcePathsChild(pathsJson, "get");
    final int numberOfNotNormalResource = this.countNotNormalResource(this.jsonExtraDocumentation);

    final int numberOfPostVerbsCreated = 4; // GET, GET-BY-ID, PATCH, DELETE
    final int numberOfGetVerbsCreated = 1; // GET-BY-ID
    final int expectedNumberOfVerb = pathsJson.length() + numberOfPost * numberOfPostVerbsCreated
            + numberOfGet * numberOfGetVerbsCreated - numberOfNotNormalResource;

    final int currentNumberOfVerb = this.countPathsChild(this.getPathsNode(jsonAfter));
    assertEquals(expectedNumberOfVerb, currentNumberOfVerb);
  }

  private int countNotNormalResource(String jsonExtraDocumentation) {
    return (int) this.loadOpenApiExtraDocumentation(jsonExtraDocumentation).stream()
            .filter(field -> !field.isNormalResource()).count();
  }

  private int countPathsChild(JSONObject pathNode) {
    int cpt = 0;
    for (final Iterator<String> i = pathNode.keys(); i.hasNext();) {
      final String pathName = i.next();
      final JSONObject path = pathNode.getJSONObject(pathName);
      cpt += path.length();
    }
    return cpt;
  }

  private int countResourcePathsChild(JSONObject pathNode, String httpVerb) {
    int cpt = 0;
    for (final Iterator<String> i = pathNode.keys(); i.hasNext();) {
      final String pathName = i.next();

      // Check if resource web service, not application, not module
      // ex: /dlcm/<module>
      if (this.countCharacter(pathName, '/') < 3) {
        continue;
      }

      // Check http verb exist
      if (pathNode.getJSONObject(pathName).isNull(httpVerb)) {
        continue;
      }

      cpt++;
    }
    return cpt;
  }

}
