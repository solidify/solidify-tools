# Purpose

This maven plugin allow to clean openapi v3 json file by :
- removing hash at the end of schema name
- force type of array to string
- update the reference to point on the new schema name
- remove servlet context name ("/dlcm") as prefix on api paths