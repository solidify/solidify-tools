/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - ParametersObject.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import ch.unige.solidify.process.BaseProcess;

public class ParametersObject {
  public class SchemaObject {
    private AdditionalPropertiesObject additionalProperties;
    private String format;
    private String type;

    private SchemaObject() {
    }

    public AdditionalPropertiesObject getAdditionalProperties() {
      return this.additionalProperties;
    }

    public String getFormat() {
      return this.format;
    }

    public String getType() {
      return this.type;
    }

    public void setAdditionalProperties(AdditionalPropertiesObject additionalProperties) {
      this.additionalProperties = additionalProperties;
    }

    public void setFormat(String format) {
      this.format = format;
    }

    public void setType(String type) {
      this.type = type;
    }

  }

  public static ParametersObject getFiltersFormQueryParameters() {
    final ParametersObject parametersObject = new ParametersObject();
    parametersObject.in = BaseProcess.QUERY;
    parametersObject.name = BaseProcess.FILTERS;
    parametersObject.schema.type = BaseProcess.OBJECT;
    parametersObject.schema.additionalProperties = new AdditionalPropertiesObject(BaseProcess.STRING);
    parametersObject.description = "Filters forms to add where condition. To filter on a field if the field is embedded in a sub structure, the field name must be fully named with “.” for each level:+ <sub structure name>.<field name>";
    parametersObject.style = BaseProcess.FORM;
    parametersObject.required = false;
    parametersObject.explode = true;
    return parametersObject;
  }

  private String description;
  private boolean explode;
  private String in;
  private String name;
  private boolean required;

  private final SchemaObject schema;

  private String style;

  public ParametersObject() {
    this.in = "path";
    this.name = "resId";
    this.description = "Resource Id (GUID)";
    this.required = true;
    this.schema = new SchemaObject();
    this.schema.type = "string";
  }

  public ParametersObject(String name, String in, String description, boolean required, String type) {
    this.in = in;
    this.name = name;
    this.description = description;
    this.required = required;
    this.schema = new SchemaObject();
    this.schema.type = type;
  }

  public ParametersObject(String name, String in, String description, boolean required, String type, String format) {
    this(name, in, description, required, type);
    this.schema.format = format;
  }

  public String getDescription() {
    return this.description;
  }

  public String getIn() {
    return this.in;
  }

  public String getName() {
    return this.name;
  }

  public SchemaObject getSchema() {
    return this.schema;
  }

  public String getStyle() {
    return this.style;
  }

  public boolean isExplode() {
    return this.explode;
  }

  public boolean isRequired() {
    return this.required;
  }

}
