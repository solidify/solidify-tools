/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify asciidoctor TOC Extractor - AsciidoctorTocExtracterTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.JAXBException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.unige.solidify.AsciidoctorTocExtracter;
import ch.unige.solidify.process.TocExtractionException;

import junit.framework.TestCase;

@ExtendWith(MockitoExtension.class)
public class AsciidoctorTocExtracterTest extends TestCase {

  private static String SRC_TEST_RESOURCES = "src/test/resources/";
  private static String DLCM_GUIDE = SRC_TEST_RESOURCES + "DLCM-IntegrationGuide.html";
  private static String DLCM_GUIDE_TOC = SRC_TEST_RESOURCES + "DLCM-IntegrationGuide-toc.html";
  private final AsciidoctorTocExtracter tocExtracter = new AsciidoctorTocExtracter();

  @Test
  public void testRun() throws Exception {
    try {
      this.tocExtracter.load(DLCM_GUIDE);
      this.tocExtracter.run(null);
      this.tocExtracter.generate();
      final Path toc = Paths.get(DLCM_GUIDE_TOC);
      assertTrue(toc.toFile().exists());
      Files.delete(toc);
    } catch (JAXBException | IOException | TocExtractionException e) {
      throw e;
    }

  }

}
