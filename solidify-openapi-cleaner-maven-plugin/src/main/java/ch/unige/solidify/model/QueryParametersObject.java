/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - QueryParametersObject.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.util.ArrayList;
import java.util.List;

import ch.unige.solidify.process.BaseProcess;

public class QueryParametersObject {
  private final List<ParametersObject> parameters;

  public QueryParametersObject() {
    this.parameters = new ArrayList<>();
    this.parameters.add(new ParametersObject(
            BaseProcess.SIZE,
            BaseProcess.QUERY,
            "The page size",
            false,
            BaseProcess.INTEGER,
            BaseProcess.FORMAT_INTEGER_INT32));
    this.parameters.add(new ParametersObject(
            BaseProcess.PAGE,
            BaseProcess.QUERY,
            "The current page number",
            false,
            BaseProcess.INTEGER,
            BaseProcess.FORMAT_INTEGER_INT32));
    this.parameters.add(new ParametersObject(
            BaseProcess.SORT,
            BaseProcess.QUERY,
            "To sort on a field. By default, the sort is ascending. desc option '[,desc]' permits to have descending order.",
            false,
            BaseProcess.STRING));
  }

  public List<ParametersObject> getParameters() {
    return this.parameters;
  }

}
