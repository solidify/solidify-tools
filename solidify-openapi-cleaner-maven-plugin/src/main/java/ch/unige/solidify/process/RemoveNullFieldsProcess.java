/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - RemoveNullFieldsProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Update tags with module name
 */
public class RemoveNullFieldsProcess extends BaseProcess {

  @Override
  public void process(JSONObject json) {
    for (final Iterator<String> i = json.keys(); i.hasNext();) {
      final String field = i.next();
      this.removeNullFields(json.get(field));
    }
  }

  private void removeNullFields(Object o) {
    if (o instanceof JSONArray) {
      final JSONArray array = (JSONArray) o;
      for (int i = 0; i < array.length(); ++i) {
        this.removeNullFields(array.get(i));
      }
    } else if (o instanceof JSONObject) {
      final JSONObject json = (JSONObject) o;
      final JSONArray names = json.names();
      if (names == null) {
        return;
      }
      for (int i = 0; i < names.length(); ++i) {
        final String key = names.getString(i);
        if (json.isNull(key)) {
          json.remove(key);
        } else {
          this.removeNullFields(json.get(key));
        }
      }
    }
  }

}
