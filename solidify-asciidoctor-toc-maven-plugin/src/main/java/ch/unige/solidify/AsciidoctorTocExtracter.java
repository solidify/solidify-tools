/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify asciidoctor TOC Extractor - AsciidoctorTocExtracter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.apache.maven.plugin.logging.Log;

import ch.unige.solidify.model.xml.xhtml.v1.XhtmlHtmlType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlUlType;
import ch.unige.solidify.process.HrefUpdateProcess;
import ch.unige.solidify.process.TocExtractionException;
import ch.unige.solidify.process.TocExtractionProcess;

public class AsciidoctorTocExtracter {
  private Path file;
  private JAXBContext jaxbContext;
  JAXBElement<XhtmlHtmlType> html;
  JAXBElement<XhtmlUlType> toc;

  public void generate() throws JAXBException, IOException {
    this.saveHtml(this.getTocName(this.file));
  }

  public void load(String inputFilePath) throws JAXBException, IOException {
    this.jaxbContext = JAXBContext.newInstance(XhtmlHtmlType.class, XhtmlUlType.class);
    this.file = Paths.get(inputFilePath);
    this.html = this.loadHtml(this.file);
  }

  public void run(Log log) throws TocExtractionException {
    // ******************************
    // ** !! Order is important !! **
    // ******************************
    // Extract TOC
    this.log(log, "Extracting TOC from HTML document");
    this.toc = new TocExtractionProcess().process(this.html, this.toc);
    // Update links
    this.log(log, "Updating links in HTML document");
    this.toc = new HrefUpdateProcess(this.file.getFileName().toString()).process(this.html, this.toc);
  }

  private String getNewName(Path file, String suffix) {
    return file.getParent().toString() + File.separatorChar
            + file.getFileName().toString().replaceFirst(".html", "-" + suffix + ".html");
  }

  private Path getTocName(Path file) {
    return Paths.get(this.getNewName(file, "toc"));
  }

  private JAXBElement<XhtmlHtmlType> loadHtml(Path file) throws IOException, JAXBException {
    JAXBElement<XhtmlHtmlType> root = null;
    // Load HTML Document
    try (FileReader fileReader = new FileReader(file.toFile(), StandardCharsets.UTF_8)) {
      root = (JAXBElement<XhtmlHtmlType>) this.jaxbContext.createUnmarshaller().unmarshal(fileReader);
    } catch (IOException | JAXBException e) {
      this.log(null, e.getMessage());
      throw e;
    }
    return root;
  }

  @SuppressWarnings("squid:S106")
  private void log(Log log, String message) {
    if (log == null) {
      System.out.println(message);
    } else {
      log.info(message);
    }
  }

  private void saveHtml(Path outputFile) throws JAXBException, IOException {
    try (FileWriter fileWriter = new FileWriter(outputFile.toFile(), StandardCharsets.UTF_8)) {
      this.jaxbContext.createMarshaller().marshal(this.toc, fileWriter);
    } catch (JAXBException | IOException e) {
      this.log(null, e.getMessage());
      throw e;
    }
  }
}
