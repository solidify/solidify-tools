/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - SchemaCollectionAndPageCreationProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import org.json.JSONObject;

import ch.unige.solidify.model.CollectionObject;
import ch.unige.solidify.model.PageObject;

/**
 * Add to schema collection and page
 */
public class SchemaCollectionAndPageCreationProcess extends BaseProcess {

  @Override
  public void process(JSONObject json) {
    // Add collection info in schemas
    final CollectionObject collectionObject = new CollectionObject();
    final JSONObject collectionObjectJsonObject = this.convertObjectAsJsonObject(collectionObject);
    this.getSchemasNode(json).put(COLLECTION_NAME, collectionObjectJsonObject);
    // Add page info in schemas
    final PageObject pageObject = new PageObject();
    final JSONObject pageObjectJsonObject = this.convertObjectAsJsonObject(pageObject);
    this.getSchemasNode(json).put(PAGE_NAME, pageObjectJsonObject);
  }
}
