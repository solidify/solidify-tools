/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - PathWithIdCreationProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import ch.unige.solidify.model.ParametersObject;

/**
 * Add new path with id (for GET, PATCH, DELETE)
 */
public class PathWithIdCreationProcess extends BaseProcess {
  private final JSONArray paramatersJsonArray;

  public PathWithIdCreationProcess() {
    this.paramatersJsonArray = this.getParametersJsonArray();
  }

  @Override
  public void process(JSONObject json) {
    final Map<String, JSONObject> pathsWithId = new HashMap<>();
    this.iterateOverPostPaths(json, pathsWithId);
    json.put(PATHS_KEY, this.mergeJsonObject(this.getPathsNode(json), new JSONObject(pathsWithId)));
  }

  private void addPathWithId(Map<String, JSONObject> pathsWithId, String pathName,
          JSONObject postPath, boolean readOnlyResource) {
    final JSONObject pathsWithIdContent = new JSONObject();
    this.createVerbs(postPath, pathsWithIdContent, readOnlyResource);
    pathsWithId.put(pathName + URL_SEPARATOR + RES_ID_URL_PARAM, pathsWithIdContent);
  }

  private void createDeletePath(JSONObject pathsWithIdContent, JSONObject deletePath) {
    deletePath.remove(REQUEST_BODY_KEY);
    deletePath.put(PARAMETERS_KEY, this.paramatersJsonArray);
    pathsWithIdContent.put(DELETE_PATH, deletePath);
  }

  private void createGetPath(JSONObject pathsWithIdContent, JSONObject getPath) {
    getPath.remove(REQUEST_BODY_KEY);
    getPath.put(PARAMETERS_KEY, this.paramatersJsonArray);
    pathsWithIdContent.put(GET_PATH, getPath);
  }

  private void createPatchPath(JSONObject pathsWithIdContent, JSONObject patchPath) {
    patchPath.put(PARAMETERS_KEY, this.paramatersJsonArray);
    pathsWithIdContent.put(PATCH_PATH, patchPath);
  }

  private void createVerbs(JSONObject httpPath, JSONObject pathsWithIdContent,
          boolean readOnlyResource) {
    this.createGetPath(pathsWithIdContent, this.deepCopy(httpPath));
    if (!readOnlyResource) {
      this.createPatchPath(pathsWithIdContent, this.deepCopy(httpPath));
      this.createDeletePath(pathsWithIdContent, this.deepCopy(httpPath));
    }
  }

  private JSONObject getHttpVerb(JSONObject path) {
    final JSONObject post = (JSONObject) path.query(QUERY_JSON_PREFIX + POST_PATH);
    if (post != null) {
      return post;
    }
    return (JSONObject) path.query(QUERY_JSON_PREFIX + GET_PATH);
  }

  private JSONObject getParameterJsonObject() {
    return new JSONObject(this.getParametersJson());
  }

  private String getParametersJson() {
    return this.toJson(new ParametersObject());
  }

  private JSONArray getParametersJsonArray() {
    final JSONArray jsonArray = new JSONArray();
    jsonArray.put(this.getParameterJsonObject());
    return jsonArray;
  }

  private void iterateOverPostPaths(JSONObject json, Map<String, JSONObject> pathsWithId) {
    for (final Iterator<String> i = this.getPathsNode(json).keys(); i.hasNext();) {
      final String pathName = i.next();
      final JSONObject path = this.getPathsNode(json).getJSONObject(pathName);

      // Check if resource web service, not application, not module
      if (this.countCharacter(pathName, '/') != 1) {

        final boolean readOnlyResource = this.isReadOnlyResource(path);

        // Get http verb template
        JSONObject httpPath = this.getHttpVerb(path);
        if (httpPath != null) {

          // Update response to 200
          if (!readOnlyResource) {
            httpPath = this.updateResponse(httpPath, "201", "200");
          }

          // Add new web services (GET by ID, DELETE, PATH)
          this.addPathWithId(pathsWithId, pathName, httpPath, readOnlyResource);
        }
      }
    }
  }
}
