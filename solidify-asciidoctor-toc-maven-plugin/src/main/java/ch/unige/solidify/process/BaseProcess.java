/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify asciidoctor TOC Extractor - BaseProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import javax.xml.bind.JAXBElement;

import ch.unige.solidify.model.xml.xhtml.v1.XhtmlHtmlType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlUlType;

@SuppressWarnings("squid:S2068")
public abstract class BaseProcess {

  public BaseProcess() {
  }

  public abstract JAXBElement<XhtmlUlType> process(JAXBElement<XhtmlHtmlType> html, JAXBElement<XhtmlUlType> toc) throws TocExtractionException;

}
