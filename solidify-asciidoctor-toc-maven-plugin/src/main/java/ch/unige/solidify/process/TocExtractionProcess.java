/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify asciidoctor TOC Extractor - TocExtractionProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.process;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.JAXBElement;

import ch.unige.solidify.model.xml.xhtml.v1.XhtmlDivType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlHtmlType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlUlType;

public class TocExtractionProcess extends BaseProcess {

  @Override
  public JAXBElement<XhtmlUlType> process(JAXBElement<XhtmlHtmlType> html, JAXBElement<XhtmlUlType> toc) throws TocExtractionException {
    // Extract TOC by removing content
    if (html.getValue() == null) {
      throw new TocExtractionException("Cannot load HTML document");
    }
    if (html.getValue().getBody() == null) {
      throw new TocExtractionException("Cannot load HTML body");
    }
    if (html.getValue().getBody().getXhtmlBlockMix() == null) {
      throw new TocExtractionException("Cannot load HTML body blocks");
    }
    for (final Serializable item : html.getValue().getBody().getXhtmlBlockMix()) {
      if (item instanceof XhtmlDivType) {
        final XhtmlDivType div = (XhtmlDivType) item;
        if (div.getId().equals("header")) {
          return this.processHeader(div.getContent());
        }
      }
    }
    throw new TocExtractionException("Cannot find HTML header");
  }

  private JAXBElement<XhtmlUlType> processHeader(List<Serializable> content) throws TocExtractionException {
    for (final Serializable item : content) {
      if (item instanceof JAXBElement<?>) {
        final Object obj = ((JAXBElement<?>) item).getValue();
        if (obj instanceof XhtmlDivType) {
          final XhtmlDivType div = (XhtmlDivType) obj;
          if (div.getId().equals("toc")) {
            return this.processToc(div.getContent());
          }
        }
      }
    }
    throw new TocExtractionException("Cannot find TOC");
  }

  private JAXBElement<XhtmlUlType> processToc(List<Serializable> content) throws TocExtractionException {
    for (final Serializable item : content) {
      if (item instanceof JAXBElement<?>) {
        final Object obj = ((JAXBElement<?>) item).getValue();
        if (obj instanceof XhtmlUlType) {
          final XhtmlUlType ul = (XhtmlUlType) obj;
          if (ul.getClazz() != null && ul.getClazz().equals("sectlevel1")) {
            return (JAXBElement<XhtmlUlType>) item;
          }
        }
      }
    }
    throw new TocExtractionException("Cannot find TOC content");
  }

}
