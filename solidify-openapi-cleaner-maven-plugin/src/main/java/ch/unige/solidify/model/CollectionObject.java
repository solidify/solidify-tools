/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - CollectionObject.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import ch.unige.solidify.process.BaseProcess;

public class CollectionObject {
  public class Properties {
    private AttributeObject _data;
    private AttributeObject _links;
    private AttributeObject _page;

    public AttributeObject get_data() {
      return this._data;
    }

    public void set_data(AttributeObject _data) {
      this._data = _data;
    }

    public AttributeObject get_links() {
      return this._links;
    }

    public void set_links(AttributeObject _links) {
      this._links = _links;
    }

    public AttributeObject get_page() {
      return this._page;
    }

    public void set_page(AttributeObject _page) {
      this._page = _page;
    }

  }

  private final Properties properties = new Properties();

  private final String type;

  public CollectionObject() {
    this.properties.set_page(new AttributeObject(BaseProcess.REF_SCHEMAS_VALUE + BaseProcess.PAGE_NAME,
            "Gives the total pages and the total available items of one resource"));
    this.properties.set_links(new AttributeObject(BaseProcess.OBJECT, "All links available on the resource"));
    this.properties.set_data(new AttributeObject(BaseProcess.OBJECT, "List the collection of the resources"));
    this.type = BaseProcess.OBJECT;
  }

  public Properties getProperties() {
    return this.properties;
  }

  public String getType() {
    return this.type;
  }

}
