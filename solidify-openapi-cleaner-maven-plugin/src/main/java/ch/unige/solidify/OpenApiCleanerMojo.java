/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Tools - Solidify OpenApi Cleaner - OpenApiCleanerMojo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import ch.unige.solidify.model.JsonCleanerException;

@Mojo(name = "clean-json", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class OpenApiCleanerMojo extends AbstractMojo {

  @Parameter(property = "contextPath", required = true)
  private String contextPath;

  @Parameter(property = "inputExtraDocumentationFile", required = true)
  private String inputExtraDocumentationFile;

  @Parameter(property = "inputFile", required = true)
  private String inputFile;

  @Parameter(property = "modules", required = true)
  private List<String> modules;

  @Parameter(property = "outputFile", required = true)
  private String outputFile;

  @Parameter(property = "securityName", required = true)
  private String securityName;

  @Parameter(property = "prefix", required = true)
  private String prefix;

  @Parameter(property = "skip", required = false, defaultValue = "false")
  private boolean skip;

  @Override
  @SuppressWarnings("squid:S1148")
  public void execute() throws MojoExecutionException {
    if (this.skip) {
      this.getLog().info("Skipping clean of openAPI json file");
      return;
    }
    final JsonCleaner jsonCleaner = new JsonCleaner();
    try {
      this.getLog().info(this.getFileReadLogMessage());
      jsonCleaner.load(this.inputFile, this.inputExtraDocumentationFile);
      this.getLog().info("Process JSON Cleanup");
      jsonCleaner.run(this.contextPath, this.modules, this.securityName, this.prefix, this.getLog());
      this.getLog().info("Write file in " + this.outputFile);
      jsonCleaner.generate(this.outputFile);
      this.getLog().info("Successfully processed JSON Object in " + this.outputFile);
    } catch (final FileNotFoundException e) {
      e.printStackTrace();
      this.getLog().info("File not found: " + e.getMessage());
      throw new MojoExecutionException(e.getMessage(), e);
    } catch (final IOException e) {
      e.printStackTrace();
      this.getLog().info("IO Exception: " + e.getMessage());
      throw new MojoExecutionException(e.getMessage(), e);
    } catch (final JsonCleanerException e) {
      e.printStackTrace();
      this.getLog().info("JsonCleaner Exception: " + e.getMessage());
      throw new MojoExecutionException(e.getMessage(), e);
    }

  }

  private String getFileReadLogMessage() {
    final StringBuilder messageReadFileBuilder = new StringBuilder();
    messageReadFileBuilder.append("Read openapi file : " + this.inputFile);
    if (this.inputExtraDocumentationFile == null) {
      messageReadFileBuilder.append(" and no extra documentation provided");
    } else {

      messageReadFileBuilder
              .append(" and extra documentation file : " + this.inputExtraDocumentationFile);
    }
    return messageReadFileBuilder.toString();
  }
}
